import {ServiceInterface} from '@whywelive/patrick-core';

/**
 * Executor is a set of services that are tied to a specific route by type of service.
 */
export interface ExecutorInterface {
  /**
   * Returns the next service from the list, if the iteration has ended, then it will be started from the start.
   */
  nextService(): ServiceInterface | undefined;

  /**
   * Adds s service to executors.
   *
   * @param service
   */
  addExecutor(service: ServiceInterface): void;

  /**
   * Removes a service from executors.
   *
   * @param service
   */
  removeExecutor(service: ServiceInterface): void;

  /**
   * Returns the set of services that process the route data.
   */
  getExecutors(): ServiceInterface[];

  /**
   * Returns the route to which this executor is bound.
   */
  getAPIRoute(): string;
}
