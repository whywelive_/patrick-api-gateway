import {ServiceInterface} from '@whywelive/patrick-core';
import * as ProxyServer from 'http-proxy';

import {Executor, ExecutorsManagerInterface} from '.';

export class ExecutorsManager implements ExecutorsManagerInterface {
  private readonly proxy: ProxyServer;

  private executors: Map<string, Executor> = new Map();

  public constructor(proxy: ProxyServer) {
    this.proxy = proxy;
  }

  /**
   * Adds service to executor by service route.
   *
   * @param service
   */
  public addExecutor(service: ServiceInterface): void {
    const key = service.getAPIRoute();

    if (!this.executors.has(key))
      this.executors.set(key, new Executor(this.proxy, service.getAPIRoute()));

    this.executors.get(key).addExecutor(service);
  }

  /**
   * Removes service from executor by service route.
   *
   * @param service
   */
  public removeExecutor(service: ServiceInterface): void {
    const key = service.getAPIRoute();

    if (this.executors.has(key)) {
      const executor = this.executors.get(key);

      executor.removeExecutor(service);

      if (executor.getExecutors().length === 0) this.executors.delete(key);
    }
  }

  /**
   * Returns executor, that will be handle request.
   *
   * @param url
   */
  public findExecutor(url: string): Executor | undefined {
    return Array.from(this.executors.values()).find(executor =>
      url.startsWith(executor.getAPIRoute()),
    );
  }
}
