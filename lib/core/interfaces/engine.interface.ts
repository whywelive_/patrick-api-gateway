import {ConfigInterface, ServiceDataInterface} from '@whywelive/patrick-core';

import {IDIConfigurationOptions} from '@tsed/di';
import {RequestHandler} from '.';

export interface EngineInterface {
  /**
   * Initialized current ApiGateway instance.
   *
   * @param expressConfig ExpressServer configuration.
   * @param config Patrick configuration.
   * @param service Patrick service configuration.
   */
  init(
    expressConfig: Partial<IDIConfigurationOptions>,
    config: ConfigInterface,
    service: ServiceDataInterface,
  ): Promise<void>;

  /**
   * This handlers will be called before proxying to service.
   *
   * @param handlers set of handlers to add
   */
  useBefore(...handlers: RequestHandler[]): void;
}
