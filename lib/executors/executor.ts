import {ServiceInterface} from '@whywelive/patrick-core';
import * as ProxyServer from 'http-proxy';

import {ExecutorInterface} from '.';

/**
 * Executor is a set of services that are tied to a specific route by type of service.
 */
export class Executor implements ExecutorInterface {
  private readonly proxy: ProxyServer;

  private readonly route: string;

  private readonly executors: ServiceInterface[] = [];

  private id = 0;

  public constructor(proxy: ProxyServer, route: string) {
    this.proxy = proxy;
    this.route = route;
  }

  /**
   * Returns the next service from the list, if the iteration has ended, then it will be started from the start.
   */
  public nextService(): ServiceInterface | undefined {
    if (this.id > this.executors.length - 1) this.id = 0;

    return this.executors[this.id++];
  }

  /**
   * Adds s service to executors.
   *
   * @param service
   */
  public addExecutor(service: ServiceInterface): void {
    this.executors.push(service);
  }

  /**
   * Removes a service from executors.
   *
   * @param service
   */
  public removeExecutor(service: ServiceInterface): void {
    const index = this.executors.findIndex(
      executor =>
        executor.getName() === service.getName() &&
        executor.getType() === service.getType(),
    );

    if (index >= 0) this.executors.splice(index, 1);
  }

  /**
   * Returns the set of services that process the route data.
   */
  public getExecutors(): ServiceInterface[] {
    return this.executors;
  }

  /**
   * Returns the route to which this executor is bound.
   */
  public getAPIRoute(): string {
    return this.route;
  }
}
