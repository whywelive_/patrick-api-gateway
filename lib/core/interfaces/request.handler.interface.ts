import {ServiceInterface} from '@whywelive/patrick-core';
import {Request, Response} from 'express';

export interface RequestHandler {
  (req: Request, res: Response, service: ServiceInterface): Promise<void>;
}
