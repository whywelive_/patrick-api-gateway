import {ServiceInterface} from '@whywelive/patrick-core';

import {Executor} from '..';

export interface ExecutorsManagerInterface {
  /**
   * Adds service to executor by service route.
   *
   * @param service
   */
  addExecutor(service: ServiceInterface): void;

  /**
   * Removes service from executor by service route.
   *
   * @param service
   */
  removeExecutor(service: ServiceInterface): void;

  /**
   * Returns executor, that will be handle request.
   *
   * @param url
   */
  findExecutor(url: string): Executor | undefined;
}
