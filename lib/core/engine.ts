import Patrick, {
  ConfigInterface,
  MessageInterface,
  MessageSourceInterface,
  ServiceDataInterface,
  ServiceInterface,
  ServiceStateInterface,
  ServiceStateType,
} from '@whywelive/patrick-core';
import {IDIConfigurationOptions} from '@tsed/di';
import {Request, Response} from 'express';
import * as ProxyServer from 'http-proxy';

import {ExecutorsManager} from '../executors';
import {asyncHandler} from '../utils';

import {EngineInterface, RequestHandler} from '.';

export class Engine implements EngineInterface {
  private readonly proxyServer: ProxyServer = ProxyServer.createProxyServer({});

  private readonly services: Map<string, ServiceInterface> = new Map();

  private readonly executorsManager: ExecutorsManager = new ExecutorsManager(
    this.proxyServer,
  );

  private readonly handlersBefore: RequestHandler[] = [];

  /**
   * Initialized current ApiGateway instance.
   *
   * @param expressConfig ExpressServer configuration.
   * @param config Patrick configuration.
   * @param service Patrick service configuration.
   */
  public async init(
    expressConfig: Partial<IDIConfigurationOptions> = {},
    config: ConfigInterface,
    service: ServiceDataInterface,
  ): Promise<void> {
    await Patrick.init(config);
    const server = await Patrick.initExpressServer(expressConfig);

    server.expressApp.all(
      '/api/*',
      asyncHandler(async (request: Request, response: Response) => {
        request.url = request.url.substring(4);

        const executors = this.executorsManager.findExecutor(request.url);

        if (!executors) throw Error('Executor not found');

        const executor = executors.nextService();

        if (!executor) throw Error('Service not found');

        await Promise.all(
          this.handlersBefore.map(handler =>
            handler(request, response, executor),
          ),
        );

        request.url = request.url.substring(executors.getAPIRoute().length);

        this.proxyServer.web(request, response, {
          target: `http://${executor.getHost()}:${executor.getPort()}`,
        });
      }),
    );

    // adds all currently available services
    this.addServices(
      await Patrick.getServiceManager()
        .getAllServices()
        .then(services =>
          services.filter(
            anotherService => anotherService.getType() !== service.type,
          ),
        ),
    );

    // handles states of new/old services
    Patrick.getMessagingManager().on(
      'message',
      async (
        channel: MessageSourceInterface,
        content: MessageInterface<ServiceStateInterface>,
      ) => {
        const {name} = content.source;

        // don't add other gateway services
        if (content.source.type === service.type) return;

        if (content.data.state === ServiceStateType.START) {
          this.addServices([
            await Patrick.getServiceManager().getServiceByName(name),
          ]);
        } else if (content.data.state === ServiceStateType.STOP) {
          this.removeService(name);
        }
      },
    );

    await Patrick.initAsService(service);
    await server.listen();
  }

  private addServices(services: ServiceInterface[]): void {
    services.forEach(service => {
      this.services.set(service.getName(), service);
      this.executorsManager.addExecutor(service);
    });
  }

  private removeService(name: string): void {
    const service = this.services.get(name);

    if (service) {
      this.executorsManager.removeExecutor(service);
      this.services.delete(name);
    }
  }

  /**
   * This handlers will be called before proxying to service.
   *
   * @param handlers set of handlers to add
   */
  public useBefore(...handlers: RequestHandler[]): void {
    this.handlersBefore.push(...handlers);
  }
}
