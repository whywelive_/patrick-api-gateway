import {RequestHandler, Request, Response, NextFunction} from 'express';

export function asyncHandler(middleware: RequestHandler): RequestHandler {
  return async (request: Request, response: Response, next: NextFunction) => {
    try {
      await middleware(request, response, next);
    } catch (error) {
      next(error);
    }
  };
}
